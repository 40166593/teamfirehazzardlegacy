﻿using UnityEngine;
using System.Collections;

public class RoomSelectHelper : MonoBehaviour 
{
    public float g_floorHeight;
    private bool m_lerpState;
    private Vector3 m_lerpEnd;
    private GameObject m_lerper;
	// Use this for initialization
	void Start () 
    {
        m_lerpState = false;
        m_lerpEnd = new Vector3(0.0f, g_floorHeight, 0.0f);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (m_lerpState)
        {
            m_lerper.transform.position = Vector3.Lerp(m_lerper.transform.position,
                                                       m_lerpEnd, Time.deltaTime *2.0f);
            if (m_lerper.transform.position.y > g_floorHeight)
            {
                m_lerpState = false;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_lerper = other.gameObject;
            m_lerpState = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        m_lerpState = false;
    }
}
