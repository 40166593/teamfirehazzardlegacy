﻿using UnityEngine;
using System.Collections;

public class OnEntry : MonoBehaviour
{
    public string statToRaise;
    public GameObject player;
    public GameObject particleSys;
    private GameObject emitParticles;

	// Use this for initialization
	void Start () 
    {
        //get a link to the player stats file
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (this.tag == "Bedroom" || this.tag == "Sleep Desk" || this.tag == "Cinema")
        {
            statToRaise = "Rest";
        }
        else if (this.tag == "Toilet" || this.tag == "Bathroom" || this.tag == "Living Room" || this.tag == "Games Room" || this.tag == "Arcade")
        {
            statToRaise = "Happy";
        }
        else if (this.tag == "Larder" || this.tag == "Kitchen" || this.tag == "Cafe" || this.tag == "Fast Food" || this.tag == "Vending Machine")
        {
            statToRaise = "Food";
        }
        else if (this.tag == "Study" || this.tag == "Machine")
        {
            statToRaise = "Cash";
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (statToRaise == "Rest")
            {
                emitParticles = (GameObject)Instantiate(particleSys, other.gameObject.transform.position + new Vector3(0.0f, 5.0f, 0.0f), transform.rotation);
                player.GetComponent<StatsBarRest>().increaseBy(5.0f);
                Debug.Log("Rest");
            }
            else if (statToRaise == "Food")
            {
                emitParticles = (GameObject)Instantiate(particleSys, other.gameObject.transform.position + new Vector3(0.0f, 5.0f, 0.0f), transform.rotation);
                Debug.Log("Food");
            }
            else if (statToRaise == "Happy")
            {
                emitParticles = (GameObject)Instantiate(particleSys, other.gameObject.transform.position + new Vector3(0.0f, 5.0f, 0.0f), transform.rotation);
                player.GetComponent<StatsBarHappy>().increaseBy(5.0f);
                Debug.Log("Happy");
            }
            else if (statToRaise == "Cash")
            {
                emitParticles = (GameObject)Instantiate(particleSys, other.gameObject.transform.position + new Vector3(0.0f, 5.0f, 0.0f), transform.rotation);
                player.GetComponent<StatsBarCash>().increaseBy(5.0f);
                Debug.Log("Cash");
            }
        }
    }
}
