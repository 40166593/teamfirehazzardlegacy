﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class RoomSelector : MonoBehaviour
    {
        public GameObject g_world;
        public AudioClip upSrc;

        private float m_verticalMove;
        private bool m_insideRoom;
        private Quaternion m_targetLerp;
        private Quaternion m_forward;
        private bool m_lerpState;
        private AudioSource audio;
        private GameObject currentRoom;

        void Start()
        {
            m_verticalMove = 0;
            m_insideRoom = false;
            m_targetLerp = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
            m_forward = transform.rotation;
            m_lerpState = false;
            audio = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            if (m_insideRoom && m_verticalMove == 0)
            {
                m_verticalMove = CrossPlatformInputManager.GetAxis("Vertical");

                if (m_verticalMove != 0)
                {
                    Time.timeScale = 0.3f;
                    m_lerpState = true;
                    audio.PlayOneShot(upSrc, 1.0f);
                }
            }
            if (m_lerpState)
            {
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation,
                                                          m_targetLerp, 0.1f);
                if (transform.rotation.y <= m_targetLerp.y)
                {
                    m_lerpState = false;
                }
            }
            else
            {
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation,
                                                          m_forward, 0.1f);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "RoomSelector")
            {
                m_insideRoom = true;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "RoomSelector")
            {
                if (other.GetComponent<NeighboursRooms>().finalRoom)
                {
                    m_verticalMove = -1;
                }
                if (m_verticalMove > 0 && other.GetComponent<NeighboursRooms>().upperRoom)
                {
                    transform.position = new Vector3(0.0f, other.GetComponent<NeighboursRooms>().upperRoom.transform.position.y, 0.0f);
                    transform.rotation = m_forward;
                }
                else if ((m_verticalMove < 0 && other.GetComponent<NeighboursRooms>().lowerRoom))
                {
                    transform.position = new Vector3(0.0f, other.GetComponent<NeighboursRooms>().lowerRoom.transform.position.y, 0.0f);
                    transform.rotation = m_forward;
                }
                Time.timeScale = 1.0f;
                m_insideRoom = false;
                m_lerpState = false;
                m_verticalMove = 0;
            }
        }
    }
}