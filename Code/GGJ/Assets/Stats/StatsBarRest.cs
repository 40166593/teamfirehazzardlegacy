﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsBarRest : MonoBehaviour {

	public Image Bar;
	public float max_value = 100.0f;
	public float min_value = 0.0f;
	public float cur_value = 0.0f;

	// Use this for initialization
	void Start () {
		
		cur_value = max_value;
		InvokeRepeating ("deplete", 0.0f, 1.5f);
	}

	private void deplete(){

		decreaseBy(1.5f);
	}

	void decreaseBy(float value){

        if ((cur_value - value) < 0)
        {
            cur_value = 0;
        }
        else
        {
            cur_value -= value;
        }

        if (cur_value == 0)
        {
            Application.LoadLevel("GameOver");
        }

		Mathf.Clamp (cur_value, min_value, max_value);

		float cur_stat = cur_value / max_value;

		SetStat(cur_stat);
	}

	public void increaseBy(float value){
		
		if ((cur_value + value) > 100)
        {
            cur_value = 100;
        }
        else
        {
            cur_value += value;
        }
        
		Mathf.Clamp (cur_value, min_value, max_value);

		float cur_stat = cur_value / max_value;

		SetStat(cur_stat);
	}

	void SetStat(float myStat){

		Bar.fillAmount = myStat;
	}
}
